#! /bin/bash

if [[ $1 == "test" ]]
then
  PSQL="psql --username=postgres --dbname=worldcuptest -t --no-align -c"
else
  PSQL="psql --username=freecodecamp --dbname=worldcup -t --no-align -c"
fi

# Do not change code above this line. Use the PSQL variable above to query your database.

echo "$($PSQL "TRUNCATE TABLE games, teams;")"

tail -n +2 games.csv | while IFS="," read YEAR ROUND WINNER OPP WINNER_GOALS OPP_GOALS
do
  #ADD EACH UNIQUE TEAM
  echo "$($PSQL "INSERT INTO teams(name) VALUES('$WINNER') ON CONFLICT DO NOTHING;")"
  echo "$($PSQL "INSERT INTO teams(name) VALUES('$OPP') ON CONFLICT DO NOTHING;")"

  WINNER_ID="$($PSQL "SELECT team_id FROM teams WHERE name='$WINNER';")"
  OPP_ID="$($PSQL "SELECT team_id FROM teams WHERE name='$OPP';")"

  # Make sure to add the correct ID's from the teams table (you cannot hard-code the values)

  echo "$($PSQL "INSERT INTO games(year, round, winner_id, opponent_id, winner_goals, opponent_goals) 
  VALUES($YEAR, '$ROUND', $WINNER_ID, $OPP_ID, $WINNER_GOALS, $OPP_GOALS);")"
done
